use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'FaCT++';

run_ok 1, $CMD;
cmp_ok stdout, 'eq', '', 'Testing stdout';
like stderr, qr/Usage:\s+FaCT\+\+ <Conf file>  or\n\s+FaCT\+\+ -get-default-options/, 'Testing stderr';

run_ok $CMD, qw/-get-default-options/;
like stdout, qr/Option 'useSpecialDomains'/, 'Testing stdout';
like stderr, qr/FaCT\+\+\.Kernel: Reasoner for the SROIQ\(D\) Description Logic/, 'Testing stderr';

done_testing;
